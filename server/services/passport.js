const JwtStrategy=require('passport-jwt').Strategy;
const ExtractJwt=require('passport-jwt').ExtractJwt;
const LocalStrategy=require('passport-local')
const passport=require('passport');
const User=require('../database/model/user')
const config=require('../config/secret');
const { response } = require('express');


const jwtOption={
    jwtFromRequest:ExtractJwt.fromHeader('authorization'),
    secretOrKey:config.secret

}

const JwtSt=new JwtStrategy(jwtOption, (payload,done)=>{

    

    User.findById(payload.sub, function(err,user){

        if(err){

            return done(err)
        }

        if(!user){
            return done(null,false)
        }

        done(null,user)
    })

})


const localOption={
    usernameField:"email"
}

const LocalSt=new LocalStrategy(localOption, (username,password,done)=>{


    User.findOne({email:username}, (err,user)=>{

        if(!user){

            return done(null,false)
        }

        if(err){
            return done(err)
        }

        user.comparePassword(password,function(err,isMatch){

            if(err){

                return done(err)
            }

            if(!isMatch){

                return done(null,false)
            }


            done(null,user)



        })

    })

})

passport.use(LocalSt)
passport.use(JwtSt)

