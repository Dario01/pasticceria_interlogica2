const Authentication=require('../../server/authentication/auth')
const Ingredients=require('../database/ingredients/ingredientsMethods')
const Cakes=require('../database/cakes/cakesMethods')
//const SalesPriceList=require('../database/salesPriceList/salesPriceListMethods')
const passport = require('passport')
const passportService=require('../services/passport')


const requireLogin=passport.authenticate('local',{session:false})
const requireAuth=passport.authenticate('jwt',{session:false})

module.exports=app=>{


    app.get("/api/logged", requireAuth,(req,res)=>{

        
        res.send(req.user)
    })


    app.post("/api/login",Authentication.login)

    app.post("/api/register", Authentication.register)

    app.post("/api/insert/ingredients", Ingredients.inserting)

    app.get("/api/reading/ingredients", Ingredients.reading)

    app.post("/api/insert/cakes",Cakes.inserting)
    
    app.get("/api/reading/cakes", Cakes.reading)

    //app.post("/api/insert/salespricelist", SalesPriceList.inserting)

}