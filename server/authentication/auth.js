const User=require('../database/model/user')
const jwt=require('jwt-simple')
const config=require('../config/secret')

function tokenForUser(user){

    console.log(user)

    let timestamp=new Date().getTime()

return jwt.encode({sub:user._id, iat:timestamp}, config.secret)

}

exports.register=(req,res,next)=>{

    console.log('req body',req.body)

    if(!req.body.email  || !req.body.password ){

       return res.status(422).send({error:'Please provide an email and password'})
    }

    User.findOne({email:req.body.email}, (err,existingUser)=>{

        if(existingUser){
        
            return res.send({error:'This user already exists'})
           
        }

        if(err){
            console.log('Errore')
           return next(err)
        }
        const user=new User({
            email:req.body.email,
            password:req.body.password
        })
        user.save()
        res.send(user)
    })
}


exports.login=(req,res,next)=>{

    console.log('REQQI',req)

    User.findOne({email:req.body.email}, function(err,user){

        if(err){

            return next(err)
        }

        if(!user){

           return res.send({error:'Wrong credentials'})
        }

        user.comparePassword(req.body.password, function(err,isMatch){

            if(err){
                return next(err)
            }
            if(!isMatch){

                return res.status(422).send({error:'Wrong credentials'})
            }

            res.send({token:tokenForUser(user)})
        })

    })

}