const Ingredient=require('../model/ingredient');
const jwt=require('jwt-simple');
const config=require('../../../server/config/secret');
const User = require('../model/user');



exports.inserting=(req,res,next)=>{

   

        try{

    var decoded=jwt.decode(req.body.token,config.secret)
        }catch(e){
            
            res.send({error:'Token errato, riaggiorna la pagina'})
        }
    

    User.findById(decoded.sub, function(err,user){

        if(err){
            return res.send({error:err})
        }
        if(!user){
            return res.send({error:'This user not exists'})
        }

        if(user){

            Ingredient.findOne({name:req.body.ingredientData.name}, function(err,existingIngredient){

                if(err){
    
                    return next(err)
                }
                if(existingIngredient){
                    return res.send({error:"This ingredient already exist"})
                }
    
                const ingredient=new Ingredient({
                    name:req.body.ingredientData.name
                })
                ingredient.save()
                res.send(ingredient)
    
        })

        }
    })

}

exports.reading=(req,res,next)=>{

    try{
        var decoded=jwt.decode(req.headers.authorization,config.secret)
    }catch(e){
        res.send({error:'Token errato, aggiorna la pagina', e})
    }

    User.findById(decoded.sub, function(err,user){

        if(err){
            return res.send({error:err})
        }
        if(!user){
            return res.send({error:'This user does not exists'})
        }

        if(user){


        Ingredient.find({}, function(err,result){


            if(err){
                return next(err)
            }
    
            res.send(result)
    
        })
    }


    })


}