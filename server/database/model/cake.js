const mongoose=require('mongoose');
const Ingredient = require('./ingredient');
const {Schema}=mongoose;



const CakeSchema=new Schema({
    name:String,
    ingredients:'',
    weight:String

})

CakeSchema.virtual('ingredientCount').get(function(){
    return this.ingredients.length
})

const Cake=mongoose.model('cake',CakeSchema)

module.exports=Cake