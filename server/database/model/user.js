const mongoose=require('mongoose')
const {Schema}=mongoose;
const bcrypt=require('bcrypt')


const UserSchema=new Schema({
    email: String,
    password:String
})


UserSchema.methods.comparePassword=function(candidatePassword,callback){


    bcrypt.compare(candidatePassword, this.password, (err, isMatch)=>{

        if(err){

            return callback(err)
        }

        if(!isMatch){

            return callback(null,false)
        }

        callback(null,isMatch)
    })


}

UserSchema.pre('save',function(next){

    const user=this

    bcrypt.genSalt(8,  function(err,salt){

        if(err){
            return next(err)
        }

        bcrypt.hash(user.password,salt, function(err,hash){

            if(err){
                return next(err)
            }

            user.password=hash
            next()

        })


        
    })


})


const User=mongoose.model('users',UserSchema)

module.exports=User