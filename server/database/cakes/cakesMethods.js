const Cake=require('../model/cake')
const jwt=require('jwt-simple');
const config=require('../../../server/config/secret');
const User=require('../model/user')


exports.inserting=(req,res,next)=>{


    console.log('REQ CAKES',req.body)

    try{
        var decoded=jwt.decode(req.body.token,config.secret)

        console.log('DECODED',decoded)

    }catch(e){
        res.send({error:'Token non valido, riaggiorna la pagina'})

    }

User.findById(decoded.sub, function(err,existingUser){

    if(err){
        return res.send({error:err})
    }
    if(!existingUser){
        return res.send({error:'This user does not exists'})
    }

    Cake.findOne({name:req.body.name}, function(err, existingCake){

        if(err){

            next(err)
        }
        if(existingCake){
            return res.send({error:"This cake already exist"})
        }

        const cake=new Cake({

          name:req.body.name,
          ingredients:req.body.ingredients,
          weight:req.body.weight 
        })
        cake.save()

        res.send(cake)

    })
})
}

exports.reading=(req,res,next)=>{

    Cake.find({}, function(err,result){

        if(err){
            return res.send({error:err})
        }
        if(!result){
            return res.send({error:'There is not any cakes'})
        }

        res.send({cakes:result})
    })




}