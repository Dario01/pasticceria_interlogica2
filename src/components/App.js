import React, {useEffect} from 'react';
import {BrowserRouter,Route} from 'react-router-dom';
import Header from './Header';
import {connect} from 'react-redux';
import * as actions from '../redux/actions/index';
import AuthRoutes from './routes/AuthRoutes';
import PublicRoutes from './routes/PublicRoutes';




function App(props){

    

    useEffect(()=>{

            props.autoLogin()
            
    },[])


    function logout(){
        props.logout(props.history)
    }





    return(
<BrowserRouter>
        <div className="container">

           
            <Header logout={logout}/>
            {props.auth ?
            <div>
            <Route path="/" component={AuthRoutes}/>
            </div>
            :
            <div>
            <Route path="/" component={PublicRoutes}/>
            </div>
        }
            
            
        </div>
        </BrowserRouter>
    )


}

function mapStateToProps(state){

    return{
        auth:state.auth.token
    }
}

export default connect(mapStateToProps,actions)(App);