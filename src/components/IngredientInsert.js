import React, {useState} from 'react';
import {connect} from 'react-redux'
import * as actions from '../redux/actions/index'
import {withRouter} from 'react-router-dom';
import axios from 'axios'


function IngredientInsert(props){

   

    const [ingredientData, setIngredientData]=useState({name:'', result:''})

    function changeData(e){
        
        setIngredientData({name:e.target.value})
    }

    async function insertIngredient(){
        const obj={ingredientData, token:localStorage.getItem('token')}

            const res=await axios.post("/api/insert/ingredients",obj)
            console.log('RES SU INGREDIENTS',res)

            if(res.data.name){
                
                return setIngredientData({...ingredientData,result:'Insert OK'})
            }else{

                
                return setIngredientData({...ingredientData,result:res.data.error})
            }

        }

    return(
        <div>
        <input type="text" value={ingredientData.name} onChange={changeData} />
        <button onClick={insertIngredient}>Insert Ingredient</button>
        <div>
        <h6>{ingredientData.result}</h6>
        </div>
        </div>

    )

}

export default withRouter(IngredientInsert);