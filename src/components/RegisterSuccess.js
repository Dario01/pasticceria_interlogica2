import React from 'react';
import {Link} from 'react-router-dom'


function RegisterSuccess(){


    return(
        <div>
            <h3>Registration Completed</h3>
            <p>Please click the link below to return in Homepage</p>
            <Link to="/">Home</Link>
        </div>



    )
}

export default RegisterSuccess;