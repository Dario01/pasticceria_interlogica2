import React, {useState, useEffect} from 'react';
import IngredientInsert from './IngredientInsert'
import CakeInsert from './CakeInsert';
import SalesSellInsert from './SalesSellInsert'



function AdministratorPage(){

    console.log('ADMINISTRATOR PAGE')

    const [viewData, setViewData]=useState({cake:false,ingredient:false, sellCake:false})

    function changeView(e){

        if(e.target.name==='cake'){

        setViewData({cake:true,ingredient:false, sellCake:false})
    }
        if(e.target.name==='ingredient'){

            setViewData({ingredient:true,cake:false, sellCake:false})
        }
        if(e.target.name==='sellCake'){
            setViewData({cake:false, ingredient:false, sellCake:true})
        }
    }

    useEffect(() => {

        console.log('state view',viewData)
        
    })


    function renderFields(){
        if(viewData.ingredient){
            return <div><IngredientInsert/></div>
      
        }if(viewData.cake){
            return <div><CakeInsert/></div>
        }if(viewData.sellCake){
            return <div><SalesSellInsert/></div>
        }
    }
    


    

    return(
        <div>
            <button onClick={changeView} name="ingredient">Insert Ingredients</button>
            <button onClick={changeView} name="cake">Insert new Cake</button>
            <button onClick={changeView} name="sellCake">Sell your cakes</button>

            
            {renderFields()}




        </div>


    )
}

export default AdministratorPage;



