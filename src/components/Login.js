import React, {useState} from 'react';
import {connect} from 'react-redux'
import * as actions from '../redux/actions/index'
import {withRouter} from 'react-router-dom'


function Login(props){

    const [userData,setUserData]=useState({email:'', password:''})

    function changeData(e){
        

        setUserData({...userData, [e.target.name]:e.target.value})
    }

    function login(e){
        e.preventDefault()

        props.login(userData,props.history)

    }

    
    

    return(
       <div>
        <form onSubmit={login}>
            <h1>Login</h1>
            <div className="form-group">
    <label>Email address</label>
    <input type="email" className="form-control" name="email" placeholder="Email" value={userData.email} onChange={changeData}>
        </input>
    </div>
    <div className="form-group">
    <label >Password</label>
    <input type="password" className="form-control" name="password" placeholder="Password" value={userData.password} onChange={changeData}>
        </input>
  </div>
  
  <button type="submit" className="btn btn-primary">Submit</button>
  
</form>
<div>
    <p>{props.error}</p>
</div>
</div>
    )
}

function mapStateToProps(state){
    
    return{
        error:state.auth.error
    }
}

export default connect(mapStateToProps,actions)(withRouter(Login));