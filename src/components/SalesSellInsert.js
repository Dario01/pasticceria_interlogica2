import React,{useEffect,useState} from 'react';
import * as actions from '../redux/actions/index';
import {connect} from 'react-redux'



function SalesSellInsert(props){

    function setDisable(e){

        console.log(e.target.name,e.target.checked)

        props.setDisable(e.target.value)


    }

    
    useEffect(()=>{

        props.readingCakes()

        
    },[])
    console.log('PROPS CAKES',props.cakes)

    return(
<div>
        <div>
            <h3>Give a price to your cakes</h3>
        </div>
        <div>
        {props.cakes.cakes.map((cake)=>{
                console.log('INSIDE MAP',cake.id)
return (
    <div key={cake.id}>
    <input
                
                type="checkbox"
                name="name"
                value={cake.cake}
                onClick={(e)=>setDisable(e)}
              />
              <label>
                <b>{cake.cake}</b>
              </label>
             
                 {cake.disable ?
                 <div key={cake.id}>
                 <input type="text" name="first" disabled={cake.disable}/>
                 <label>Prezzo in prima</label>
                 <input type="text" name="second" disabled={cake.disable}/>
                 <label>Prezzo in seconda</label>
                 <input type="text" name="third" disabled={cake.disable}/>
                 <label>Prezzo in terza</label>
                 </div>
                 :
                 null  
                }

                 
              </div>
        )})}

    </div>
    </div>
        
    )
}

function mapStateToProps(state){
    console.log('STATE',state)

    return{
        cakes:state.cakes
    }

}

export default connect(mapStateToProps,actions)(SalesSellInsert);