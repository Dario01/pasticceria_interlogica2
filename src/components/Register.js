import React,{useState, useEffect} from 'react';
import * as actions from '../redux/actions/index';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'



function Register(props){

    const [userData,setUserData]=useState({email:'', password:''})

      
    function changeData(e){

        setUserData({...userData,[e.target.name]:e.target.value})
    
    }

    function registration(e){
        e.preventDefault();

        props.register(userData,props.history)
    }


    useEffect(()=>{


        console.log(userData)
    })



    return(
       <div>
        <form onSubmit={registration}>
            <h1>Registrati</h1>
            <div className="form-group">
    <label>Email address</label>
    <input type="email" className="form-control" name="email" placeholder="Email" value={userData.name} onChange={changeData}/>
        
    </div>
    <div className="form-group">
    <label >Password</label>
    <input type="password" className="form-control" name="password" placeholder="Password" value={userData.password} onChange={changeData}/>
        
  </div>
  
  <button type="submit" className="btn btn-primary">Submit</button>
  
</form>
<div>
    <p>{props.error}</p>
</div>
</div>
    )
}

function mapStateToProps(state){
    return{
        error:state.auth.error
    }
}

export default connect(mapStateToProps,actions)(withRouter(Register));