import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom'
import AdministratorPage from '../AdministratorPage';



function AuthRoutes(){


    return(
        <div className="auth">
            <Switch>
        <Route exact path="/administratorPage" component={AdministratorPage}/>
        <Redirect from="*" to="/administratorPage"/>
        </Switch>
        </div>


    )
}

export default AuthRoutes;