import React from 'react';
import {Route,Redirect,Switch} from 'react-router-dom'
import StartPage from '../StartPage';
import Login from '../Login';
import Register from '../Register';




function PublicRoutes(){

    console.log('PUBLIC')

    return(
        <div className="public">
            <Switch>
            <Route exact path="/" component={StartPage}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Redirect from="*" to="/"/>
            </Switch>
            </div>


    )
}

export default PublicRoutes