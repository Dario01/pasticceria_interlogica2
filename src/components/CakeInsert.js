import React, { useEffect, useState } from "react";
import axios from "axios";

function CakeInsert(props) {
  var [listIngredient, setListIngredients] = useState([{ name: '' }]);
  var [cakeData, setCakeData] = useState({ name: '',weight:'', result:'' });
  let arrayIngredients = [];


  function resetCheckBox(){
      var clist=document.querySelectorAll('input[type="checkbox"]')
      for (var i=0;i<clist.length;i++){

        clist[i].checked=false
      }
      console.log('clist',clist)
  }
  

  function changeData(e) {
    setCakeData({ ...cakeData, [e.target.name]:e.target.value });
    console.log(cakeData)
  }

  async function insertCake() {
    let obj={name:cakeData.name, weight:cakeData.weight, ingredients:arrayIngredients, token:localStorage.getItem('token')}

    const res=await axios.post("/api/insert/cakes", obj)

    console.log('RES CAKE',res)

    if (res.data.error){

        return setCakeData({...cakeData, result:res.data.error})
    
    }else{
        resetCheckBox()
        return setCakeData({name:'',weight:'',result:''})
    }

  }

  function insertIngredientsInCake(e) {
    if (e.target.checked) {
      arrayIngredients.push(e.target.name);
    } else {
      const elem = arrayIngredients.indexOf(e.target.name);

      arrayIngredients.splice(elem, 1);
    }
  }
  useEffect(() => {



    async function readingIngredients() {

     

        const headers={
            'Content-type':'application/json',
            'authorization':localStorage.getItem('token')
        }
  
   

      const res = await axios.get("/api/reading/ingredients",{
        headers:headers
      });


      if (res.data.error){

        return setCakeData({...cakeData, result:res.data.error})

     
    }else{

      setListIngredients((listIngredient = res.data));

    }

  }

    readingIngredients();

  
  }, []);

  return (
    <div>
      <input type="text" onChange={changeData} name="name" placeholder="Name Cake" value={cakeData.name}></input>
      <input type="text" onChange={changeData} name="weight" placeholder="Weight" value={cakeData.weight}></input>
      <button onClick={insertCake}>Insert Cake</button>
      <div>
        {listIngredient.map((ingredient) => {
          return (
            <div key={ingredient.name}>
              <input
                type="checkbox"
                name={ingredient.name}
                onClick={(e) => insertIngredientsInCake(e)}
              />
              <label>
                <b>{ingredient.name}</b>
              </label>
            </div>
          );
        })}

        <h6>{cakeData.result}</h6>
      </div>
    </div>
  );
}

export default CakeInsert;
