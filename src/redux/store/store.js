import {createStore} from 'redux'
import reducers from '../reducers/index'
import {applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk'

const store=createStore(reducers,applyMiddleware(reduxThunk))

export default store;