import {SET_USER, SET_ERROR, SET_TOKEN, SET_INIT, SET_CAKE, SET_DISABLE} from './type'
import axios from 'axios';


export const autoLogin=()=>async dispatch=>{

    

    if(localStorage.getItem('token')){

        try{

        const headers={
            'Content-type':'application/json',
            'authorization':localStorage.getItem('token')
        }

       const res=await axios.get("/api/logged",{
           headers:headers
           
       })

        

        if(res){
            
            
            dispatch({type:SET_TOKEN, payload:localStorage.getItem('token')})
        }
        
    }catch(e){

        
        localStorage.removeItem('token')
        
    }
}

}


export const register= (values, history)=>async (dispatch)=>{

    
    const res=await axios.post("/api/register", values)

    

    if(res.data.error){

        dispatch({type:SET_ERROR, payload:res.data.error})

    }else{

        history.push("/success")
    
    }
}


export const logout=()=>dispatch=>{

    localStorage.removeItem('token')
    dispatch({type:SET_TOKEN})

}


export const login=(values, history)=>async (dispatch)=>{

    

    const res=await axios.post("/api/login", values)

   

    if(res.data.error!==undefined){
       
        dispatch({type:SET_ERROR, payload:res.data.error})
    }else{
        
        localStorage.setItem('token',res.data.token)
        history.push("/administratorPage")
        dispatch({type:SET_TOKEN, payload:res.data.token})
        dispatch({type:SET_INIT, payload:true})  
    }
}


export const readingCakes=()=>async (dispatch)=>{

    const res=await axios.get("/api/reading/cakes")

    

    dispatch({type:SET_CAKE, payload:res.data.cakes})

}

export const setDisable=(value)=>dispatch=>{

    dispatch({type:SET_DISABLE, payload:value})
}