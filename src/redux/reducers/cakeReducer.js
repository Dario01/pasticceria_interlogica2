import {SET_CAKE, SET_ERROR, SET_DISABLE} from '../actions/type'
import { bindActionCreators } from 'redux'

const initialState={
    cakes:[{}],
    error:undefined,
    
}



export default function(state=initialState, action){

    switch (action.type){
        case SET_CAKE:
            return{
                ...state, cakes:action.payload.map(cake=>{
                    return{
                        id:cake._id,
                        cake:cake.name,
                        disable:false
                    }
                })
            }
        case SET_DISABLE:
            

            state.cakes.forEach(elem=>{

                if(elem.cake===action.payload){
                   
                    elem.disable=!elem.disable
                }
            })

            return{
               ...state
            }
case SET_ERROR:
    return{
        ...state, error:action.payload
    }
            default:
                return state
    }
}