import { SET_USER, SET_ERROR, SET_TOKEN,SET_INIT } from "../actions/type"



const initialState={
    user:undefined,
    token:undefined,
    error:undefined,
    init:false
}


export default function(state=initialState, action){

    switch(action.type){
        case SET_INIT:
            return{...state, init:true}
        case SET_TOKEN:
            return{...state,token:action.payload}

        case SET_ERROR:
            return{...state, error:action.payload}
            default:
                return state
    }



}