import {combineReducers} from 'redux';
import authReducer from './authReducer';
import cakeReducer from './cakeReducer';



export default combineReducers({

    auth:authReducer,
    cakes:cakeReducer
    
})